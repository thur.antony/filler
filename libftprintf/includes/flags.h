/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 17:29:56 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/12 17:29:58 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FLAGS_H
# define FLAGS_H

# define PER		0X80000000
/*
**	-- flags --
*/
# define HASH		0X40000000
# define ZERO		0X20000000
# define MINUS		0X10000000
# define SPACE      0X8000000
# define PLUS       0X4000000
/*
**	-- options --
*/
# define WIDTH		0X2000000
# define PRECISION	0X1000000
/*
**	-- length --
*/
# define HH			0X800000
# define H			0X400000
# define L			0X200000
# define LL			0X100000
# define J			0X80000
# define Z			0X40000
/*
**	-- types --
*/
# define S			0X8000
# define WS			0X208000
# define C			0X4000
# define WC			0X204000
# define PTR		0X2000
# define D			0X1000
# define I			0X1000
# define O			0X800
# define U			0X400
# define X			0X200
# define X_UP		0X300
# define X_UP_CHECK	0X100
# define B			0X20

# define LD			0X201000
# define LO			0X200800
# define LU			0X200400
/*
**	-- unicode --
*/
# define ACC_H_TWO	0X1F00
# define ACC_L_TWO	0X3F
# define MASK_H_TWO	0XC0
# define MASK_L 0X80

# define ACC_H_TR	0XF000
# define ACC_M_TR	0XFC0
# define ACC_L_TR	ACC_L_TWO
# define MASK_H_TR	0XE0

# define ACC_H_F	0X1C0000
# define ACC_N_F	0X3F000
# define ACC_M_F	ACC_M_TR
# define ACC_L_F	ACC_L_TWO
# define MASK_H_F	0XF0

# define UNUSED(x) (void)(x)

#endif
