/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/04 13:23:12 by avykhova          #+#    #+#             */
/*   Updated: 2018/01/04 13:23:14 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_putnbr_fd(int n, int fd)
{
	long	nn;

	nn = (long)n;
	if (nn < 0)
	{
		ft_putchar_fd('-', fd);
		nn *= -1;
	}
	if (nn < 10)
		ft_putchar_fd(nn + 48, fd);
	else
	{
		ft_putnbr_fd(nn / 10, fd);
		ft_putchar_fd((nn % 10) + 48, fd);
	}
}
