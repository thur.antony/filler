/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/22 16:20:22 by avykhova          #+#    #+#             */
/*   Updated: 2018/01/04 13:59:33 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strequ(char const *s1, char const *s2)
{
	int		res_of_comparison;

	res_of_comparison = 0;
	if (s1 && s2)
	{
		if (ft_strlen(s1) != ft_strlen(s2))
			return (0);
		while (*s2 != '\0' && res_of_comparison == 0)
			res_of_comparison = *s1++ - *s2++;
		if (res_of_comparison == 0)
			return (1);
	}
	return (0);
}
