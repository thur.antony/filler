/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/18 13:16:05 by avykhova          #+#    #+#             */
/*   Updated: 2017/12/18 13:55:07 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strncmp(const char *s1, const char *s2, size_t len)
{
	size_t	i;

	i = 0;
	while (s1[i] && i < len && !((int)(s1[i] - s2[i])))
		++i;
	if (i == len)
		i--;
	return ((int)(((unsigned char *)s1)[i] - ((unsigned char *)s2)[i]));
}
