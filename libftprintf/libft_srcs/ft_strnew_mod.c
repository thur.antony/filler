/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew_mod.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 15:21:15 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 15:21:20 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew_mod(size_t size, char c)
{
	char	*s;

	s = (char *)malloc(sizeof(char) * (size + 1));
	if (s)
	{
		ft_memset(s, c, size);
		s[size] = '\0';
		return (s);
	}
	return (NULL);
}
