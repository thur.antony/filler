/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_concat.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 15:22:35 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 15:22:41 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_str_concat(const char *s1, const char *s2)
{
	char	*res;
	int		size[3];
	int		i;

	size[1] = ft_strlen(s1);
	size[2] = ft_strlen(s2);
	size[0] = size[1] + size[2];
	res = ft_strnew(size[0]);
	i = 0;
	while (i < size[1])
	{
		res[i] = s1[i];
		i++;
	}
	while (i < size[0])
	{
		res[i] = s2[i - size[1]];
		i++;
	}
	return (res);
}
