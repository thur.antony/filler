/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_setters.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 13:59:14 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/12 13:59:16 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					set_i0(t_arg *o, int i0)
{
	o->i0 = i0;
}

void					set_i1(t_arg *o, int i1)
{
	o->i1 = i1;
}

void					set_width(t_arg *o, int width)
{
	o->width_dt = width;
}

void					set_precision(t_arg *o, int precision)
{
	o->precision_dt = precision;
}
