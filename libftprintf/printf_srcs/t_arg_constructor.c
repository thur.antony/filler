/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/11 22:34:02 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/11 22:34:04 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_arg					*new_arg(void)
{
	t_arg				*new;

	if (!(new = (t_arg *)malloc(sizeof(t_arg))))
		return (NULL);
	new->bitmap = 0;
	new->width_dt = 0;
	new->precision_dt = 0;
	new->i0 = -1;
	new->i1 = -1;
	new->initial_width = -1;
	new->exact_width = -1;
	new->dynamic_content = 0;
	new->content = NULL;
	new->type = NULL;
	new->precision = NULL;
	new->width = NULL;
	new->flags = NULL;
	return (new);
}

void					del_arg(t_arg *o)
{
	if (o->dynamic_content)
		free(o->content);
	if (o != NULL)
		free(o);
}
