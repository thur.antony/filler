/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl_d.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:15:34 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:15:37 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					d_type(t_arg *o, va_list *ap)
{
	if (o->bitmap & L)
		o->container.d = va_arg(*ap, long long);
	else if (o->bitmap & HH)
		o->container.d = (char)va_arg(*ap, int);
	else if (o->bitmap & H)
		o->container.d = (short)va_arg(*ap, int);
	else if (o->bitmap & L || o->bitmap & J)
		o->container.d = va_arg(*ap, long);
	else if (o->bitmap & LL)
		o->container.d = va_arg(*ap, long long);
	else if (o->bitmap & Z)
		o->container.d = va_arg(*ap, size_t);
	else
		o->container.d = va_arg(*ap, int);
	o->content = ft_itoa_base(o->container.d, 10);
	o->exact_width = ft_strlen(o->content);
	o->dynamic_content = 1;
}

void					d_precision(t_arg *o)
{
	if (o->bitmap & PRECISION)
	{
		if (o->container.d < 0)
			d_prec_modify_neg(o);
		else
			d_prec_modify_pos(o);
	}
}

void					d_flags(t_arg *o)
{
	char				*tmp;

	if (o->container.d >= 0)
	{
		if (o->bitmap & PLUS)
		{
			tmp = ft_str_concat("+", o->content);
			o->exact_width++;
			free(o->content);
			o->content = tmp;
		}
		else if (o->bitmap & SPACE)
		{
			tmp = ft_str_concat(" ", o->content);
			o->exact_width++;
			free(o->content);
			o->content = tmp;
		}
	}
}

void					d_width(t_arg *o)
{
	if (o->bitmap & WIDTH && o->width_dt > o->exact_width)
	{
		if (o->bitmap & MINUS)
			d_width_modify_minus(o);
		else if (o->bitmap & ZERO && !(o->bitmap & PRECISION))
			d_width_modify_zero(o);
		else
			d_width_modify_simple(o);
	}
}

void					d_vtbl(t_arg *o)
{
	o->type = &d_type;
	o->precision = &d_precision;
	o->flags = &d_flags;
	o->width = &d_width;
}
