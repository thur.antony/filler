/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl_ls.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:16:43 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:16:45 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					ls_type(t_arg *o, va_list *ap)
{
	o->container.ls = va_arg(*ap, int *);
	if (o->container.ls == NULL)
	{
		o->content = ft_strdup("(null)");
		o->exact_width = 6;
	}
	else if (o->container.ls[0] == 0)
	{
		o->content = ft_strdup("(null)");
		o->exact_width = 6;
	}
	o->dynamic_content = 1;
}

void					ls_unicode(t_arg *o)
{
	int					i;
	int					content_i;
	int					size;

	i = 0;
	size = 0;
	content_i = 0;
	if (o->container.ls == NULL)
		return ;
	o->exact_width = 0;
	while (o->container.ls[i])
	{
		size = get_char_num(get_binary_size(o->container.ls[i]));
		o->exact_width += size;
		content_realloc(o, o->exact_width);
		apply_mask_ls(o, i, content_i, size);
		content_i += size;
		i++;
	}
	o->container.ls = 0;
	o->container.s = o->content;
}

void					ls_width(t_arg *o)
{
	int					diff;
	char				*dst;
	char				*res;

	if (!(o->bitmap & WIDTH) || o->width_dt <= o->exact_width)
		return ;
	diff = o->width_dt - o->exact_width;
	dst = ft_strnew_mod(diff, ' ');
	if (o->bitmap & MINUS)
		res = ft_str_concat(o->container.s, dst);
	else
		res = ft_str_concat(dst, o->container.s);
	if (o->dynamic_content == 1)
		free(o->container.s);
	free(dst);
	o->container.s = res;
	o->dynamic_content = 1;
	o->exact_width = ft_strlen(o->container.s);
	o->content = o->container.s;
}

void					ls_vtbl(t_arg *o)
{
	o->type = &ls_type;
	o->precision = &ls_unicode;
	o->width = &ls_width;
	o->flags = NULL;
}
