/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl_p.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:17:21 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:17:23 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					p_type(t_arg *o, va_list *ap)
{
	o->container.u = va_arg(*ap, unsigned long);
	o->dynamic_content = 1;
	o->content = ft_itoa_base_u_low(o->container.u, 16);
	o->exact_width = ft_strlen(o->content);
	change_bitmap(o, HASH);
}

void					p_vtbl(t_arg *o)
{
	o->type = &p_type;
	o->precision = &u_precision;
	o->flags = &x_flags;
	o->width = &x_width;
}
