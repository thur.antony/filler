/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/05 18:07:43 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/05 18:07:44 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

# include "../libftprintf/includes/ft_printf.h"

typedef struct			s_filler_obj
{
	char				ms_new;
	char				ms_old;
	char				es_new;
	char				es_old;
	int					map_width;
	int					map_height;
	int					fymn;
	int					fymx;
	int					fxmn;
	int					fxmx;
	int					figure_width;
	int					figure_height;
	int					**heat_map;
	char				**figure;
	char				*temp_str;
	int					g;
}						t_filler_obj;

void					algorithm(t_filler_obj *o, int y, int x);
int						place_valid(t_filler_obj *o, int y, int x);

void					del_temp_str(t_filler_obj *o);
void					del_figure(t_filler_obj *o);
void					del_heat_map(t_filler_obj *o);
void					exit_game(t_filler_obj *o);
void					get_my_number(t_filler_obj *o);
void					get_map_size(t_filler_obj *o);
void					heat_map_init(t_filler_obj *o);
void					map_to_heat_map(t_filler_obj *o, int m);
void					fill_heat_map(t_filler_obj *o);
void					accept_new_figure(t_filler_obj *o);
void					fig_limits(t_filler_obj *o);

void					horisontal_fill(t_filler_obj *o, int y, int x, int m);
void					vertical_fill(t_filler_obj *o, int y, int x, int m);
void					diagonal_up(t_filler_obj *o, int y, int x, int i);
void					diagonal_down(t_filler_obj *o, int y, int x, int i);

#endif
