/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algorithm.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/05 17:59:41 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/05 17:59:42 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void					algorithm(t_filler_obj *o, int y, int x)
{
	int					coords[2];
	int					new_sum;
	int					old_sum;

	old_sum = 0;
	y = -1;
	while (++y <= o->map_height - o->figure_height)
	{
		x = -1;
		while (++x <= o->map_width - o->figure_width)
		{
			if ((new_sum = place_valid(o, y, x)))
			{
				if (old_sum == 0 || new_sum < old_sum)
				{
					coords[0] = y;
					coords[1] = x;
					old_sum = new_sum;
				}
			}
		}
	}
	ft_printf("%d %d\n", coords[0], coords[1]);
}

int						place_valid(t_filler_obj *o, int y, int x)
{
	int					over;
	int					fy;
	int					fx;
	int					sum;

	over = 0;
	fy = -1;
	sum = 0;
	while (++fy < o->figure_height)
	{
		fx = -1;
		while (++fx < o->figure_width)
			if (o->figure[fy][fx] == '*')
			{
				if (o->heat_map[y + fy][x + fx] < 0)
					o->heat_map[y + fy][x + fx] == -1 ? (over++) : (over += 2);
				else
					sum += o->heat_map[y + fy][x + fx];
			}
	}
	if (over == 1)
		return (sum);
	else
		return (0);
}
