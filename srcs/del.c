/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   del.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/05 17:59:47 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/05 17:59:49 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void					del_temp_str(t_filler_obj *o)
{
	if (o->temp_str)
	{
		free(o->temp_str);
		o->temp_str = NULL;
	}
}

void					del_figure(t_filler_obj *o)
{
	int					y;

	y = -1;
	while (++y < o->figure_height)
	{
		free(o->figure[y]);
		o->figure[y] = NULL;
	}
	free(o->figure);
	o->figure = NULL;
}

void					del_heat_map(t_filler_obj *o)
{
	int					y;

	y = -1;
	while (++y < o->map_height)
		free(o->heat_map[y]);
	free(o->heat_map);
}

void					exit_game(t_filler_obj *o)
{
	del_heat_map(o);
	exit(1);
}
