/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/05 18:00:19 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/05 18:00:21 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int					main(void)
{
	t_filler_obj	o;

	get_my_number(&o);
	get_map_size(&o);
	heat_map_init(&o);
	map_to_heat_map(&o, 1);
	fill_heat_map(&o);
	accept_new_figure(&o);
	while (1)
	{
		algorithm(&o, -1, -1);
		del_figure(&o);
		map_to_heat_map(&o, 2);
		fill_heat_map(&o);
		accept_new_figure(&o);
	}
	return (0);
}
