/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game_loop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/05 18:00:05 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/05 18:00:07 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void					map_to_heat_map(t_filler_obj *o, int m)
{
	int					x;
	int					y;
	int					i;

	while (m--)
		(!(get_next_line(0, &o->temp_str))) ? exit_game(o) : del_temp_str(o);
	y = -1;
	while (++y < o->map_height)
	{
		get_next_line(0, &o->temp_str);
		x = -1;
		i = -1;
		while (o->temp_str[++i] != ' ')
			;
		while (++x < o->map_width)
			if (o->temp_str[++i] == '.')
				o->heat_map[y][x] = 0;
			else if (o->temp_str[i] == o->ms_new || o->temp_str[i] == o->ms_old)
				o->heat_map[y][x] = -1;
			else if (o->temp_str[i] == o->es_new || o->temp_str[i] == o->es_old)
				o->heat_map[y][x] = -2;
		del_temp_str(o);
	}
}

void					fill_heat_map(t_filler_obj *o)
{
	int					y;
	int					x;

	y = -1;
	while (++y < o->map_height)
	{
		x = -1;
		while (++x < o->map_width)
		{
			if (o->heat_map[y][x] == -2)
			{
				horisontal_fill(o, y, x, 1);
				vertical_fill(o, y, x, 1);
				diagonal_up(o, y, x, 1);
				diagonal_down(o, y, x, 1);
			}
		}
	}
}

void					fig_limits(t_filler_obj *o)
{
	int					x;
	int					y;

	y = -1;
	o->fxmn = 0;
	o->fxmx = 0;
	o->fymn = 0;
	o->fymx = 0;
	while (++y < o->figure_height)
	{
		x = -1;
		while (++x < o->figure_width)
			if (o->figure[y][x] == '*')
			{
				if (o->fxmn == 0 || x < o->fxmn)
					o->fxmn = x;
				if (o->fymn == 0 || y < o->fymn)
					o->fymn = y;
				if (x > o->fxmx)
					o->fxmx = x;
				if (y > o->fymx)
					o->fymx = y;
			}
	}
}

void					accept_new_figure(t_filler_obj *o)
{
	int					i;
	int					j;

	i = 6;
	get_next_line(0, &o->temp_str);
	o->figure_height = ft_atoi(&o->temp_str[i]);
	while (o->temp_str[++i] != ' ')
		;
	o->figure_width = ft_atoi(&o->temp_str[i]);
	del_temp_str(o);
	o->figure = (char **)malloc(sizeof(char *) * o->figure_height);
	i = -1;
	while (++i < o->figure_height)
	{
		get_next_line(0, &o->temp_str);
		j = -1;
		o->figure[i] = ft_strnew_mod(o->figure_width, '<');
		while (o->figure[i][++j])
			o->figure[i][j] = o->temp_str[j];
		del_temp_str(o);
	}
	fig_limits(o);
}
