/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/05 17:59:58 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/05 17:59:59 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void					get_my_number(t_filler_obj *o)
{
	int					my_number;

	if (get_next_line(0, &o->temp_str))
	{
		my_number = (int)(o->temp_str[10] - 48);
		del_temp_str(o);
		if (my_number == 1)
		{
			o->ms_new = 'o';
			o->ms_old = 'O';
			o->es_new = 'x';
			o->es_old = 'X';
		}
		else
		{
			o->ms_new = 'x';
			o->ms_old = 'X';
			o->es_new = 'o';
			o->es_old = 'O';
		}
	}
	else
		exit(1);
}

void					get_map_size(t_filler_obj *o)
{
	int					i;

	i = 8;
	if (!get_next_line(0, &o->temp_str))
		exit(1);
	if (ft_strlen(o->temp_str) < 10)
		exit(1);
	o->map_height = ft_atoi(&o->temp_str[i]);
	while (o->temp_str[i] != ' ')
		i++;
	o->map_width = ft_atoi(&o->temp_str[++i]);
	del_temp_str(o);
}

void					heat_map_init(t_filler_obj *o)
{
	int					y;

	o->heat_map = (int **)malloc(sizeof(int *) * o->map_height);
	y = -1;
	while (++y < o->map_height)
		o->heat_map[y] = (int *)malloc(sizeof(int) * o->map_width);
}
