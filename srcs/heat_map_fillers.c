/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heat_map_fillers.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/05 18:00:12 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/05 18:00:14 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void					horisontal_fill(t_filler_obj *o, int y, int x, int m)
{
	int					x_r;
	int					x_l;
	int					i;

	x_l = x - 1;
	i = m;
	while (x_l >= 0)
	{
		if (o->heat_map[y][x_l] == 0 || o->heat_map[y][x_l] > i)
			o->heat_map[y][x_l] = i;
		i++;
		x_l--;
	}
	x_r = x + 1;
	i = m;
	while (x_r < o->map_width)
	{
		if (o->heat_map[y][x_r] == 0 || o->heat_map[y][x_r] > i)
			o->heat_map[y][x_r] = i;
		i++;
		x_r++;
	}
}

void					vertical_fill(t_filler_obj *o, int y, int x, int m)
{
	int					y_up;
	int					y_down;
	int					i;

	y_up = y - 1;
	i = m;
	while (y_up >= 0)
	{
		if (o->heat_map[y_up][x] == 0 || o->heat_map[y_up][x] > i)
			o->heat_map[y_up][x] = i;
		i++;
		y_up--;
	}
	y_down = y + 1;
	i = m;
	while (y_down < o->map_height)
	{
		if (o->heat_map[y_down][x] == 0 || o->heat_map[y_down][x] > i)
			o->heat_map[y_down][x] = i;
		i++;
		y_down++;
	}
}

void					diagonal_up(t_filler_obj *o, int y, int x, int i)
{
	int					xt;
	int					yt;
	int					it;

	xt = x;
	yt = y;
	it = i;
	while (--x >= 0 && --y >= 0)
	{
		if (o->heat_map[y][x] == 0 || o->heat_map[y][x] > i)
			o->heat_map[y][x] = i;
		horisontal_fill(o, y, x, i + 1);
		vertical_fill(o, y, x, i + 1);
		i++;
	}
	while (++xt < o->map_width && --yt >= 0)
	{
		if (o->heat_map[yt][xt] == 0 || o->heat_map[yt][xt] > it)
			o->heat_map[yt][xt] = it;
		horisontal_fill(o, yt, xt, it + 1);
		vertical_fill(o, yt, xt, it + 1);
		it++;
	}
}

void					diagonal_down(t_filler_obj *o, int y, int x, int i)
{
	int					xt;
	int					yt;
	int					it;

	xt = x;
	yt = y;
	it = i;
	while (--x >= 0 && ++y < o->map_height)
	{
		if (o->heat_map[y][x] == 0 || o->heat_map[y][x] > i)
			o->heat_map[y][x] = i;
		horisontal_fill(o, y, x, i + 1);
		vertical_fill(o, y, x, i + 1);
		i++;
	}
	while (++xt < o->map_width && ++yt < o->map_height)
	{
		if (o->heat_map[yt][xt] == 0 || o->heat_map[yt][xt] > it)
			o->heat_map[yt][xt] = it;
		horisontal_fill(o, yt, xt, it + 1);
		vertical_fill(o, yt, xt, it + 1);
		it++;
	}
}
